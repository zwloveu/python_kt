# type.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
Type Module
---------------

Type is a tool set to type relative logic, like type convert, type check and others with type
"""

from typing import Union

__all__ = ["is_float"]

def is_float(value: Union[str, int, float]) -> bool:
    """determin whether a value is float

    Parameters:
    -----------
        value: str/int/float, value to be checked
    Returns:
        A bool flag determines the value is floag or not
    """

    result = True
    try:
        float(value)
    except:
        result = False
    return result