# file.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
File Module
---------------

File is a tool set to handle file/path logic
"""

import asyncio
import csv
import os
from collections.abc import Generator

import aiofiles

__all__ = ["check_path_exists", "create_folder",
           "write_csv", "write_text", "write_text_async", "read_text"]


def check_path_exists(path: str) -> bool:
    """check the path is existed or not

    Parameters:
    -----------
        path: str, the path to check exist
    Returns:
        A bool determines the path is existed or not
    """

    return os.path.exists(path)


def create_folder(path: str) -> str:
    """create folder if not exists

    Parameters:
    ----------
        path: str, the folder path to be created if not exists
    Returns:
    --------
        The created folder path
    """

    if not check_path_exists(path):
        os.makedirs(path)

    return path


async def write_csv(title: list[str], content: list[list[str]], file: str,
                    mode: str = "w", encoding: str = "utf-8", newline: str = ""):
    """write content to csv file

    Parameters:
    -----------
        title: list[str], column name for csv
        content: list[list[str]], rows contente for the csv
        file: str, csv file path
        mode: str, file open mode, default `w`
        encoding: str, file content text encoding, default `utf-8`
        newline: str, file content formatting newline, default `""`
    Returns:
    --------
    """

    async def write_csv_inner():
        with open(file, mode, encoding=encoding, newline=newline) as fh:
            w = csv.writer(fh)
            w.writerow(title)
            w.writerows(content)

    await write_csv_inner()


async def write_text_async(content: list[str], file: str,
                           mode: str = "a", encoding: str = "utf-8", newline: str = "") -> None:
    """write content to text file

    Parameters:
    -----------
        content: list[str], rows contente for the text file
        file: str, test file path
        mode: str, file open mode, default `a`
        encoding: str, file content text encoding, default `utf-8`
    Returns:
    --------
    """

    async with aiofiles.open(file, mode, encoding=encoding) as fh:
        await fh.writelines(["".join([line, newline]) for line in content])


def write_text(content: list[str], file: str,
               mode: str = "a", encoding: str = "utf-8", newline: str = "") -> None:
    """write content to text file

    Parameters:
    -----------
        content: list[str], rows contente for the text file
        file: str, test file path
        mode: str, file open mode, default `a`
        encoding: str, file content text encoding, default `utf-8`
    Returns:
    --------
    """

    with open(file, mode, encoding=encoding) as fh:
        fh.writelines(["".join([line, newline]) for line in content])


def read_text(file: str, encoding: str = "utf-8", newline: str = "") -> Generator:
    """read content for a text file

    Parameters:
    -----------
        file: str, the file path
        encoding: str, file content text encoding, default `utf-8`
        newline: str, file content formatting newline, default `""`
    Returns:
    --------
        A iterator with text line of the file
    Exceptions:
    -----------
        FileNotFoundError
    """
    try:
        with open(file, "r", encoding=encoding, newline=newline) as fh:
            for line in fh:
                yield line
    except FileNotFoundError as fnne:
        raise fnne


def delete_file(file: str) -> bool:
    """delete the specified file

    Parameters:
    -----------
        file: str, file path
    Returns:
    --------
        A bool reprents delete the file successfully
    Exceptions:
    -----------
        IOError
    """

    if check_path_exists(file):
        try:
            os.remove(file)
            return True
        except IOError as e:
            raise e

    return True


if __name__ == "__main__":

    async def test():
        file_path = "text_test.txt"
        file_content: list[str] = ["Hi, Long time no see", "En...  Are u Ok?"]
        await write_text_async(file_content, file_path)
        for line in read_text(file_path):
            print(line)
        delete_file(file_path)

    asyncio.run(test())
