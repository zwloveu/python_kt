# sqlite.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
Sqlite Module
---------------

Sqlite is a tool set to handle sqlite3 logic
"""

import sqlite3

__all__ = ["create_table", "query"]


def create_table(dbpath: str, table_create_sql: str):
    """Create specified table in specified sqlite3 db

    Parameters:
    -----------
        dbpath: str, the sqlite3 db file path
        table_create_sql: str, the sql script used to create a specifed table
    Returns:
    --------
    """

    connection = connect_to_sqlite3(dbpath)

    connection.execute(table_create_sql)

    connection.close()


def connect_to_sqlite3(dbpth: str) -> sqlite3.Connection:
    """connect to sqlite3 db

    Parameters:
    -----------
        dbpath: str, the sqlite3 db file path
    Returns:
        The sqlite3.Connection instance
    """

    return sqlite3.connect(dbpth)


def query(dbpth: str, query_sql: str) -> list[tuple]:
    """query from sqlite3 db

    Parameters:
    -----------
        dbpath: str, the sqlite3 db file path
        query_sql: str, the sql to query data
    Returns:
    --------
        A list[tuple] data fetched from db, if no return empty list []
    """

    result: list = []

    connection = connect_to_sqlite3(dbpth)

    for row in connection.execute(query_sql):
        result.append(row)

    connection.close()

    return result


def execute(dbpath: str, execute_sql: str, data: tuple):
    """execute the sql with data

    Parameters:
    -----------
        dbpath: str, the sqlite3 db file path
        execute_sql: str, sql that to be executed
        data: tuple, data which the execution sql needs when execute
    Returns:
    --------
    """

    connection = connect_to_sqlite3(dbpath)

    connection.execute(execute_sql, data)

    connection.commit()

    connection.close()


def executemany(dbpath: str, execute_sql: str, data: list[tuple]):
    """execute the sql with data

    Parameters:
    -----------
        dbpath: str, the sqlite3 db file path
        execute_sql: str, sql that to be executed
        data: list[tuple], data which the execution sql needs when execute
    Returns:
    --------
    """

    connection = connect_to_sqlite3(dbpath)

    connection.executemany(execute_sql, data)

    connection.commit()

    connection.close()
