# qt5.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import sys
import types

from PyQt5.QtCore import QSize, Qt, QUrl
from PyQt5.QtGui import QFont, QIcon, QPixmap
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWidgets import (QAction, QApplication, QCheckBox, QComboBox,
                             QFileDialog, QLabel, QLineEdit, QMainWindow,
                             QProgressBar, QPushButton, QSpinBox, QTextEdit,
                             QToolBar, QWidget)


def set_label(parent: QWidget = None, object_name: str = None,
              image_file: str = None, text: str = None,
              ax: int = 0, ay: int = 0, w: int = 0, h: int = 0,
              font: QFont = None, wrap: bool = False,
              alignment: int = None) -> QLabel:
    label = QLabel()
    if parent:
        label.setParent(parent)
    if object_name:
        label.setObjectName(object_name)
    if image_file:
        try:
            with open(image_file):
                pixmap = QPixmap(image_file)
                label.setPixmap(pixmap)
        except FileNotFoundError as fnfe:
            print(f"Image \"{image_file}\" not found.")
            raise fnfe
    if text:
        label.setText(text)
    if ax > 0 or ay > 0:
        label.move(ax, ay)
    if font:
        label.setFont(font)
    if wrap:
        label.setWordWrap(True)
    if w > 0 or h > 0:
        label.resize(w, h)
    if alignment:
        label.setAlignment(alignment)

    return label


def set_qlineedit(parent: QWidget = None, object_name: str = None,
                  alignment: int = None, place_holder: str = None,
                  ax: int = 0, ay: int = 0,
                  w: int = 0, h: int = 0,
                  input_mask: str = None, min_width: int = None,
                  func: types.FunctionType = None) -> QLineEdit:
    qlineedit = QLineEdit()
    if parent:
        qlineedit.setParent(parent)
    if object_name:
        qlineedit.setObjectName(object_name)
    if alignment:
        qlineedit.setAlignment(Qt.AlignLeft)
    if ax > 0 or ay > 0:
        qlineedit.move(ax, ay)
    if w > 0 or h > 0:
        qlineedit.resize(w, h)
    if place_holder:
        qlineedit.setPlaceholderText(place_holder)
    if input_mask:
        qlineedit.setInputMask(input_mask)
    if min_width:
        qlineedit.setMinimumWidth(min_width)
    if func:
        qlineedit.returnPressed.connect(func)

    return qlineedit


def set_qpushbuttn(parent: QWidget = None, object_name: str = None,
                   text: str = None,
                   ax: int = 0, ay: int = 0, w: int = 0, h: int = 0,
                   func: types.FunctionType = None) -> QPushButton:
    qpushbutton = QPushButton()
    if parent:
        qpushbutton.setParent(parent)
    if object_name:
        qpushbutton.setObjectName(object_name)
    if text:
        qpushbutton.setText(text)
    if ax > 0 or ay > 0:
        qpushbutton.move(ax, ay)
    if w > 0 or h > 0:
        qpushbutton.resize(w, h)
    if func:
        qpushbutton.clicked.connect(func)

    return qpushbutton


def set_qcheckbox(parent: QWidget = None, object_name: str = None,
                  text: str = None, checked: bool = None,
                  ax: int = 0, ay: int = 0,
                  func: types.FunctionType = None) -> QCheckBox:
    qcheckbox = QCheckBox()
    if parent:
        qcheckbox.setParent(parent)
    if object_name:
        qcheckbox.setObjectName(object_name)
    if text:
        qcheckbox.setText(text)
    if checked:
        qcheckbox.setChecked(checked)
    if ax > 0 or ay > 0:
        qcheckbox.move(ax, ay)
    if func:
        qcheckbox.stateChanged.connect(func)

    return qcheckbox


def set_qtextedit(parent: QWidget = None, object_name: str = None,
                  ax: int = 0, ay: int = 0, w: int = 0, h: int = 0,
                  place_holder: str = None) -> QTextEdit:
    qtextedit = QTextEdit()
    if parent:
        qtextedit.setParent(parent)
    if ax > 0 or ay > 0:
        qtextedit.move(ax, ay)
    if w > 0 or h > 0:
        qtextedit.resize(w, h)
    if place_holder:
        qtextedit.setPlaceholderText(place_holder)

    return qtextedit


def set_qaction(parent: QWidget = None, object_name: str = None,
                text: str = None, shortcut: str = None,
                icon_path: str = None,
                func: types.FunctionType = None):
    qaction = QAction()
    if parent:
        qaction.setParent(parent)
    if object_name:
        qaction.setObjectName(object_name)
    if text:
        qaction.setText(text)
    if shortcut:
        qaction.setShortcut(shortcut)
    if icon_path:
        qaction.setIcon(QIcon(icon_path))
    if func:
        qaction.triggered.connect(func)

    return qaction


def save_file_dialog(parament_widget: QWidget, filter: str = "All Files (*);;Text Files (*.txt)") -> str:
    options = QFileDialog.Options()
    file_name, _ = QFileDialog.getSaveFileName(
        parament_widget, "Save File", "", filter, options=options)

    return file_name


def set_qspinbox(parent: QWidget = None, object_name: str = None,
                 ax: int = 0, ay: int = 0, w: int = 0, h: int = 0,
                 r_min: int = 0, r_max: int = 100,
                 prefix: str = None, suffix: str = None,
                 alignment: int = None) -> QSpinBox:
    qspinbox = QSpinBox()
    if parent:
        qspinbox.setParent(parent)
    if ax > 0 or ay > 0:
        qspinbox.move(ax, ay)
    if w > 0 or h > 0:
        qspinbox.resize(w, h)
    qspinbox.setRange(r_min, r_max)
    if prefix:
        qspinbox.setPrefix(prefix)
    if suffix:
        qspinbox.setSuffix(suffix)
    if alignment:
        qspinbox.setAlignment(alignment)

    return qspinbox


def set_qcombobox(parent: QWidget = None, object_name: str = None,
                  texts: list[str] = [],
                  ax: int = 0, ay: int = 0, w: int = 0, h: int = 0) -> QComboBox:
    qcombobox = QComboBox()
    if parent:
        qcombobox.setParent(parent)
    if ax > 0 or ay > 0:
        qcombobox.move(ax, ay)
    if w > 0 or h > 0:
        qcombobox.resize(w, h)
    if len(texts) > 0:
        qcombobox.addItems(texts)

    return qcombobox


def set_tool_bar(parent: QWidget = None, object_name: str = None,
                 title: str = None, icon_size: QSize = None):
    toolbar = QToolBar()
    if parent:
        toolbar.setParent(toolbar)
    if object_name:
        toolbar.setObjectName(object_name)
    if title:
        toolbar.setWindowTitle(title)
    if icon_size:
        toolbar.setIconSize(icon_size)

    return toolbar


def set_web_view(
        url: str = None,
        load_progress_func: types.FunctionType = None,
        url_changed_func: types.FunctionType = None,
        load_finished_func: types.FunctionType = None) -> QWebEngineView:
    web_view = QWebEngineView()
    if url:
        web_view.setUrl(QUrl(url))
    if load_progress_func:
        web_view.loadProgress.connect(load_progress_func)
    if url_changed_func:
        web_view.urlChanged.connect(url_changed_func)
    if load_finished_func:
        web_view.loadFinished.connect(load_finished_func)

    return web_view


def set_process_bar(parent: QWidget = None, object_name: str = None):
    process_bar = QProgressBar()
    if parent:
        process_bar.setParent(parent)
    if object_name:
        process_bar.setObjectName(object_name)

    return process_bar


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = QWidget()
    window.setGeometry(50, 50, 250, 500)
    window.setWindowTitle("Normal Window")
    set_label(window,  ax=75, ay=140, text="John Doe",
              font=QFont("Arial", 20))
    set_qpushbuttn(window, text="click me", func=window.close)
    window.show()
    sys.exit(app.exec_())
