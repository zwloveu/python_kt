# input.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
Input Module
---------------

Input is a tool set to handle user input
"""

__all__ = ["basic_int_input", "basic_input"]


def basic_int_input(str_key: str, newline: bool = False) -> int:
    """basic input on the console for int type

    Parameters:
    -----------
        str_key: str, a basic message description for the input
        newlint: bool, default False, determine to input at a new line or not
    Returns:
    --------
        A int number which user input on the screen
    """

    temp_value = basic_input(str_key, newline)
    if not temp_value.isnumeric():
        temp_value = 0
    else:
        temp_value = int(temp_value)
    return temp_value


def basic_input(str_key: str, newline: bool = False) -> str:
    """basic input on the console

    Parameters:
    -----------
        str_key: str, a basic message description for the input
        newlint: bool, default False, determine to input at a new line or not
    Returns:
    --------
        A string which user input on the screen
    """

    if newline:
        temp_value = input(f"请输入{str_key}：\n")
    else:
        temp_value = input(f"请输入{str_key}：")
    return temp_value


