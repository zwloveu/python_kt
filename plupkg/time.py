# time.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
Time Module
---------------

Time is a tool set to time/date relative logic
"""

import datetime

__all__ = ["get_local_time_value","get_local_date_value"]

def get_local_time_value():
    """return local time value

    eg: 2020-09-20 12:00:00"""
    return datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")

def get_local_date_value():
    """return local date value

    eg: 2020-09-20"""
    return datetime.datetime.now().strftime("%Y-%m-%d")

if __name__ == "__main__":
    def test():
        date_value = get_local_date_value()
        print(f"date:\t{date_value}")
        time_value = get_local_time_value()
        print(f"time:\t{time_value}")

    test()