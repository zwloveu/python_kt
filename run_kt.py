# run_kts.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

from kts.kt_20200918_18 import run_kt_20200918_18
from kts.kt_20201015_19 import run_kt_20201015_19

if __name__ == "__main__":
    # run_kt_20200918_18()
    run_kt_20201015_19()
