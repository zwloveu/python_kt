# kt_20200823_13.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
第13讲作业：
参考今天的例题，编写一个员工管理系统。
包含下面几项基本功能：
员工入职、离职、个人信息（姓名、年龄）更新。

作业提交要求：
1、脚本要结构完整，函数要有函数名
2、函数循环判断需要加注释，不要求每个语句都有，主要功能要有注释；另外就是自己觉得有助于提高代码可读性的地方也可以加注释
3、可以借鉴，不可以抄袭，抄袭一经发现，直接抄袭和被抄袭人全部打回，并且都没积分
"""

"""
5个类
    Stuff
    -----
        员工信息类

        个人信息更新

    StuffLog
    --------
        员工日志类

        入职记录

        离职记录

    StuffManageMenu
    ---------------
        员工操作菜单类

        菜单指令基本属性：
            指令CODE
            指令描述

        指令信息明细组装

    StuffManageMenuOper
    -------------------
        菜单指令操作类

        添加菜单指令

        显示菜单指令

        选择菜单指令

    StuffMangement
    --------------
        员工管理类        

        添加员工

        入职处理

        离职处理

        个人信息处理
"""




import time
class Stuff:
    """定义员工类Stuff

    属性：

        员工编号，该编号唯一

        姓名

        年龄

    方法：

        更新个人信息

        显示员工信息明细
    """

    def __init__(self, id, name, age):
        self.id = id
        self.name = name
        self.age = age

    def update(self, name, age):
        self.name = name
        self.age = age

    def toString(self):
        print(f"员工基本信息如下：\n员工编号：{self.id}\t员工姓名：{self.name}\t员工年龄：{self.age}")


class StuffLog:
    """定义员工日志类StuffLog

    属性：

        员工编号

        入职时间

        离职时间

    方法：

        入职处理日志

        离职处理日志

        状态
    """

    def __init__(self, stuffId):
        """初始化

        员工编号

        入职时间

        离职时间
        """
        self.stuff_id = stuffId
        self.entry_time = None
        self.transfer_time = None

    def entry(self):
        """入职办理日志
        """
        self.entry_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

    def transfer(self):
        """离职办理日志
        """
        if self.entry_time is None:
            print(f"员工{self.stuff_id}还未入职，无法办理离职手续")
        self.transfer_time = time.strftime(
            "%Y-%m-%d %H:%M:%S", time.localtime())

    def toString(self):
        """员工日志状态信息
        """
        if self.entry_time is None:
            print("该员工还未入职")
        else:
            if self.transfer_time is None:
                print(f"已经入职，入职时间：{self.entry_time}")
            else:
                print(f"已经离职，离职时间：{self.transfer_time}")


class StuffManageMenu:
    """定义员工管理菜单指令类StuffManageMenu

    属性：
        指令

    方法：
        指令描述
    """

    def __init__(self, commandIndex, commandDesc):
        """初始化
        """
        self.command_index = commandIndex
        self.command_desc = commandDesc

    def toString(self):
        """返回自定义的整体描述信息

        指令+指令描述
        """
        return f"{self.command_index}.{self.command_desc}"


class StuffManageMenuOper:
    """定义员工管理菜单指令操作类StuffManageMenuOper

    属性：

        菜单指令字典

        已经选择的指令(私有变量)

    方法：

        添加菜单指令

        显示菜单指令

        选择指令
    """

    def __init__(self):
        """初始化

        菜单指令字典初始化

        已经选择的菜单指令，默认0，表示什么都没有选择
        """
        self.menus = {}
        self.__selected_command = 0
        self.add_menu(1, "查询员工信息")
        self.add_menu(2, "添加员工")
        self.add_menu(3, "更新员工信息")
        self.add_menu(4, "入职办理")
        self.add_menu(5, "离职办理")
        self.add_menu(6, "退出系统")

    def add_menu(self, menuIndex, menuDesc):
        """添加菜单指令
        """
        self.menus[menuIndex] = StuffManageMenu(menuIndex, menuDesc)

    def show_menus(self):
        """显示菜单指令
        """
        print("################")
        for item in self.menus.values():
            print(item.toString())
        print("################")

    def select_menu(self) -> int:
        """选择菜单指令
        """
        while True:
            temp_selected_command = basic_int_input("指令")
            if temp_selected_command <= 0:
                continue
            else:
                if temp_selected_command not in self.menus.keys():
                    continue
                else:
                    self.__selected_command = temp_selected_command
                    break

        return self.__selected_command


class StuffMangement:
    """定义员工管理类StuffMangement

    属性：

        员工字典

        入职日志列表

        离职日志列表

    方法：

        查询员工信息

        添加员工信息

        更新员工信息

        入职办理

        离职办理
    """

    def __init__(self):
        """初始化

        员工字典

        员工日志字典
        """
        self.stuffs = {}
        self.stuff_logs = {}

    def check_stuff(self, stuffId) -> bool:
        """根据员工编号检查员工是否存在
        """
        if stuffId not in self.stuffs:
            return False
        else:
            return True

    def query_stuff(self, stuffId):
        """查询员工信息

        存在，显示员工信息

        不存在，输出错误提示：找不到该员工的信息，请您确认员工编号
        """
        if not self.check_stuff(stuffId):
            print("找不到该员工的信息，请您确认员工编号")
        else:
            self.stuffs[stuffId].toString()
            self.stuff_logs[stuffId].toString()

    def add_stuff(self, name, age):
        """添加员工个人信息
        """
        # 新的员工编号为已存在的员工编号最大值+1
        new_stuff_id = (0 if len(self.stuffs) ==
                        0 else max(self.stuffs.keys())) + 1

        self.stuffs[new_stuff_id] = Stuff(new_stuff_id, name, age)
        # 初次加入员工信息，同时也会加入员工日志信息
        self.stuff_logs[new_stuff_id] = StuffLog(new_stuff_id)
        # else:
        #     self.stuffs[stuffId].update(name, age)

        print("员工信息已经添加，信息如下：\n")
        self.query_stuff(new_stuff_id)

    def update_stuff(self, stuffId, name, age):
        """编辑员工个人信息
        """
        if not self.check_stuff(stuffId):
            print("找不到该员工的信息，请您确认员工编号")

        self.stuffs[stuffId].update(name, age)

        print("员工信息已经添加，信息如下：\n")
        self.query_stuff(stuffId)

    def entry(self, stuffId):
        """员工入职办理

        不存在，显示错误信息：找不到该员工的信息，无法办理入职手续

        存在，进行入职办理
        """
        if stuffId not in self.stuffs:
            print(f"找不到员工({stuffId})的信息，无法办理入职手续")
            return

        self.stuff_logs[stuffId].entry()

        print("员工入职已经处理，信息如下：\n")
        self.query_stuff(stuffId)

    def transfer(self, stuffId):
        """员工离职办理

        不存在，显示错误信息：找不到该员工的信息，无法办理离职手续

        存在，进行离职办理
        """
        if stuffId not in self.stuffs:
            print(f"找不到员工({stuffId})的信息，无法办理离职手续")
            return

        self.stuff_logs[stuffId].transfer()

        print("员工离职已经处理，信息如下：\n")
        self.query_stuff(stuffId)


def basic_int_input(strKey) -> int:
    """基本数字输入
    """
    temp_value = basic_str_input(strKey)
    if not temp_value.isnumeric():
        temp_value = 0
    else:
        temp_value = int(temp_value)
    return temp_value


def basic_str_input(strKey) -> str:
    """基本字符串输入
    """
    temp_value = input(f"请输入{strKey}：")
    return temp_value


def stuff_age_input():
    """员工年龄输入
    正常员工年龄在18-65岁之间
    """
    temp_value = basic_int_input("员工年龄(18-65)")

    if temp_value >= 18 and temp_value <= 65:
        return temp_value
    else:
        return 0


sm = StuffMangement()
smmo = StuffManageMenuOper()
# 一直运行，直至用户选择退出系统
while True:
    smmo.show_menus()
    command = smmo.select_menu()
    if command == 6:
        print("您已经退出系统")
        break

    if command == 1:
        # 处理查询员工信息逻辑
        while True:
            temp_stuff_id = basic_int_input("员工编号(数字)")
            if temp_stuff_id <= 0:
                continue
            else:
                sm.query_stuff(temp_stuff_id)
                break
    elif command == 2:
        # 处理添加员工信息
        temp_stuff_name = basic_str_input("员工姓名")
        while True:
            temp_stuff_age = stuff_age_input()
            if temp_stuff_age <= 0:
                continue
            else:
                sm.add_stuff(temp_stuff_name, temp_stuff_age)
                break
    elif command == 3:
        # 处理更新员工信息
        temp_stuff_id = 0
        temp_stuff_name = "default"
        temp_stuff_age = 0

        while True:
            temp_stuff_id = basic_int_input("员工编号(数字)")
            if temp_stuff_id <= 0:
                continue
            else:
                break

        if not sm.check_stuff(temp_stuff_id):
            print("找不到该员工的信息，请您确认员工编号")
        else:
            temp_stuff_name = basic_str_input("员工姓名")

            while True:
                temp_stuff_age = stuff_age_input()
                if temp_stuff_age <= 0:
                    continue
                else:
                    break


            sm.update_stuff(temp_stuff_id, temp_stuff_name, temp_stuff_age)

    elif command == 4:
        # 处理员工入职办理
        while True:
            temp_stuff_id = basic_int_input("员工编号(数字)")
            if temp_stuff_id <= 0:
                continue
            else:
                sm.entry(temp_stuff_id)
                break
    else:
        # 处理员工离职办理
        while True:
            temp_stuff_id = basic_int_input("员工编号(数字)")
            if temp_stuff_id <= 0:
                continue
            else:
                sm.transfer(temp_stuff_id)
                break
