# kt_20200823_11.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
第11讲作业：
PK小游戏：创建一个User类代表用户，然后创建2个用户，分别表示玩家和电脑。用户血量和攻击力属性值随机（例如血量50-60，攻击力10-15）。血量在创建用户时固定，攻击力在每次攻击时都随机生成。
开始后用户每按一次回车代表进行一次互相攻击，直至一方血量小于或等于零。

作业提交要求：
1、脚本要结构完整，函数要有函数名
2、函数循环判断需要加注释，不要求每个语句都有，主要功能要有注释；另外就是自己觉得有助于提高代码可读性的地方也可以加注释
3、可以借鉴，不可以抄袭，抄袭一经发现，直接抄袭和被抄袭人全部打回，并且都没积分
"""

"""
3个类：

    User
    ----
        定义用户信息

        发起攻击

    PKMenu
    ------
        定义菜单信息

        显示菜单

        选择菜单

    PK
    --
        创建角色

        开始战斗

"""

import random
from enum import Enum

# 定义游戏角色枚举类：玩家/电脑
UserRole = Enum("UserRole", ("Player", "Computer"))

# 定义用户状态枚举类：生/死
UserStatus = Enum("UserStatus", ("Live", "Dead"))


class User:
    """定义类型User(用户)

    能攻击其他用户
    """

    def __init__(self, role, name):
        """初始化

        用户角色类型：玩家/电脑

        用户血量：50-60

        用户名称

        用户状体，默认：生
        """
        # 用户角色类型：玩家/电脑
        self.role = role
        # 定义用户血量：50-60
        self.hp = random.randint(50, 61)
        # 定义用户名称
        self.name = name
        # 定义用户状体，默认：生
        self.__status = UserStatus.Live
        print(f"初始化用户，角色：{self.role.name}\t名称：{self.name}\t血量：{self.hp}")

    def attack(self, otherUser):
        """发起攻击

        自己不能攻击自己

        自身血量<=0，不能进行攻击

        目前不考虑装备/BUF对血量的影响，剩余血量 = 已有血量 - 受到的攻击
        """
        if self == otherUser:
            print("不允许自己打自己")
            return

        # 自身血量<=0，不能进行攻击
        if self.hp <= 0:
            return

        # 战斗力随机：10-15
        ce = random.randint(10, 16)

        # 暂时不考虑装备/BUF对血量的影响
        # 即剩余血量 = 已有血量 - 受到的攻击
        otherUser.hp -= ce
        print(
            f"{self.role.name}-[{self.name}]对{otherUser.role.name}-[{otherUser.name}]发起攻击，{otherUser.role.name}-[{otherUser.name}]受到{ce}伤害，血量为{otherUser.hp}")

    def check_status(self):
        """检查用户的状态

        血量>0为"生"，否则"死"
        """
        self.__status = UserStatus.Live if self.hp > 0 else UserStatus.Dead
        return self.__status


class PKMenu:
    """PK的菜单类

    指令

    指令描述

    状态，是否已经选择：0/1
    """

    def __init__(self, commandIndex, commandDesc):
        """初始化
        """
        self.command_index = commandIndex
        self.command_desc = commandDesc

    def toString(self):
        """返回自定义的整体描述信息

        指令+指令描述
        """
        return f"{self.command_index}.{self.command_desc}"


class PK:
    """PK类

    创建角色

    开始战斗
    """

    def __init__(self):
        """初始化

        菜单 menus

        用户信息 users

        已选择的指令 selected_command
        """
        self.user_len_limit = 2
        # 已选择的指令
        self.selected_command = 0
        # PK的菜单，0表示初始，1/2/3对应创建角色/开始战斗/退出
        self.menus = {1: PKMenu(1, "创建角色，目前支持单回合2个人"), 2: PKMenu(
            2, "开始战斗，不死不休"), 3: PKMenu(3, "退出")}
        self.users = {}

    def show_pk_menu(self):
        """显示指令菜单

        0 显示所有

        其他根据已选择指令进行剔除
        """
        print("请发出指令：")
        if self.selected_command == 0:
            print("################")
            for item in self.menus.values():
                print(item.toString())
            print("################")
        else:
            print(f"您已经选择的指令是：{self.menus[self.selected_command].toString()}")
            print("################")
            for item in self.menus.values():
                if item.command_index != self.selected_command:
                    print(item.toString())
            print("################")

    def select_pk_menu(self):
        """选择菜单       
        """
        while True:
            temp_selected_command = input()
            if not temp_selected_command.isnumeric():
                print("请您输入正确的数字：1/2/3")
            else:
                temp_selected_command = int(temp_selected_command)
                if temp_selected_command not in self.menus.keys():
                    print("请您输入正确的数字：1/2/3")
                else:
                    # 创建角色
                    if temp_selected_command == 1:
                        self.add_user()
                    # 开始战斗
                    elif temp_selected_command == 2:
                        self.fight()
                    # 退出
                    else:
                        self.exit()

                    break

    def exit(self):
        """退出
        """
        print("您已退出")

    def add_user(self):
        """新建角色

        目前仅支持创建两个角色进行PK
        """   

        # 每次创建用户时，发现已经存在用户数据，先进行清除
        if len(self.users) == self.user_len_limit:
            self.users.clear()
        
        while len(self.users) < self.user_len_limit:
            user_role = input("请输入用户的类型：(0代表电脑，1代表玩家)：")
            if not user_role.isnumeric():
                continue
            else:
                user_role = int(user_role)
                if user_role not in [0, 1]:
                    continue
                else:
                    user_name = input("请输入用户的名称：")
                    self.users[len(self.users)+1] = User(
                        UserRole.Computer if user_role == 0 else UserRole.Player, user_name)
        print("角色创建完毕")
        self.selected_command = 1
        # 指令运行完毕后，可以重新选择新的指令
        self.show_pk_menu()
        self.select_pk_menu()

    def fight(self):
        """战斗

        一直发起攻击，不死不休，剩余唯一一个有血的就是胜者，本逻辑没有平局，还没有考虑到同时发起攻击的情况
        """
        print("战斗开始--------------------------------")

        # 一直发起攻击，不死不休，剩余唯一一个有血的就是胜者，本逻辑没有平局，还没有考虑到同时发起攻击的情况
        the_last_live_key = 1
        while True:
            for key in self.users:
                # 用一个变量来保存其他的用户
                other_users = self.users.copy()
                # 删除调当前发起攻击的用户，剩下的就是被攻击的用户
                other_users.pop(key)
                # 用当前用户攻击其他的用户
                for otherKey in other_users:
                    self.users[key].attack(other_users[otherKey])

            the_last_live_count = 0            
            for key in self.users:
                if self.users[key].hp > 0:
                    the_last_live_count += 1
                    the_last_live_key = key

            if the_last_live_count == 1:
                break
            
        print(f"最终胜利者为：{self.users[the_last_live_key].role.name}-[{self.users[the_last_live_key].name}].")

        # 战斗结束后判断用户的血量：因为在发起攻击的时候已经判断了没有血量的用户即便发起攻击也是没有效果的
        
        print("战斗结束--------------------------------")
        self.selected_command = 2
        # 指令运行完毕后，可以重新选择新的指令
        self.show_pk_menu()
        self.select_pk_menu()


pk = PK()
pk.show_pk_menu()
pk.select_pk_menu()
