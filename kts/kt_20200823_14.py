# kt_20200823_14.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
第14讲作业：
语文老师将一些古诗存在txt文档里，一句一行。
最近，他计划抽一些古诗，自己设置一些空来让学生默写。
请你用代码帮老师完成这项工作（只要处理了一个文档，加上循环就能处理无数个文档了）。


作业提交要求：
1、脚本要结构完整，函数要有函数名
2、函数循环判断需要加注释，不要求每个语句都有，主要功能要有注释；另外就是自己觉得有助于提高代码可读性的地方也可以加注释
3、可以借鉴，不可以抄袭，抄袭一经发现，直接抄袭和被抄袭人全部打回，并且都没积分
"""

"""
对作业的理解：
1，读取一个txt文件的内容，该txt文件中有定义号的诸多古诗词
2，每个古诗中间间隔一行，古诗中的每句话间隔一行
3，读取到文件内容后，随即将每个古诗中一句话替换成空
"""




import random
import time
def read_file(filePath) -> str:
    """读取文件内容

    参数：

        filePath，文件路径

    返回值：

        文件内容

    异常：

        IOError, 文件不存在/读取失败
    """
    content = ""
    try:
        with open(filePath, "r", encoding="utf-8") as fh:
            content = fh.read()
    except IOError as err:
        print(f"Error: 没有找到文件或读取文件失败, Exception: {err.args[1]}")

    return content


def write_file(filePath, content):
    """在文件中写入内容
    """
    try:
        with open(filePath, "w", encoding="utf-8") as fh:
            fh.write(content)
    except IOError as err:
        print(f"Error: 写入文件失败, Exception: {err.args[1]}")


class PoemTrainning:
    """定义古诗培训类PoemTrainning

    属性：

        古诗字典(私有)，将每首古诗以标题(KEY)/内容列表(VALUE)的方式进行存储

    方法：

        生成作业，将每个古诗中随即空出来一句，用空行来代替
    """

    def __init__(self, filePath):
        # 古诗字典
        self.__poems = {}
        # 读取文件内容，并将文件内容用\n进行间隔，这样
        poemsList = read_file(filePath).split("\n")
        self.__build_poems(poemsList)
        if len(self.__poems) > 0:
            for k, v in self.__poems.items():
                print(k, v)

    def __build_poems(self, poemsList):
        """将列表中古诗内容进行组装，
        该方法将每首古诗存储在古诗字典__poems中
        """
        if len(poemsList) == 0:
            return

        # 去除列表中元素多余的空格
        poemsList = list(map(lambda item: item.strip(), poemsList))

        if max([len(item) for item in poemsList]) == 0:
            return

        # 组装古诗字典
        while True:
            # 获取第一个空元素的位置，在这里进行截取
            new_line_index = poemsList.index("")
            # 古诗标题是第一个元素，其余的在空元素的位置前的都是古诗内容
            if len(poemsList[0]) > 0:
                self.__poems[poemsList[0]] = poemsList[1:new_line_index]

            # 删除第一个元素到空元素位置的列表元素
            del poemsList[:new_line_index + 1]

            if len(poemsList) == 0:
                break

    def train(self, count):
        """生成作业

        遍历古诗字典，对每首古诗的内容中随机一句话替换为空行
        """
        if len(self.__poems) > 0:
            trainPoem = {}
            for k, v in self.__poems.items():
                if len(v) > 0:
                    toBeEmptyLine = random.randint(0, len(v)-1)
                    v[toBeEmptyLine] = "***********"
                    trainPoem[k] = v

            fileContent = ""
            if len(trainPoem) > 0:
                for k, v in trainPoem.items():
                    print(k, v)
                    peomContent = k + "\n" + "\n".join(v) + "\n"
                    fileContent += peomContent + "\n"

            # 写入文件
            if count < 1:
                count = 1
            for i in range(count):
                fileName = "poems.train." + \
                    time.strftime("%Y-%m-%d_%H:%M:%S",
                                  time.localtime()) + "_" + str(i + 1)
                write_file(f"./temp/{fileName}.txt", fileContent)


pt = PoemTrainning("./resources/poems.txt")
print("-----------------------------------------------------------------------")
pt.train(10)
print("-----------------------------------------------------------------------")
