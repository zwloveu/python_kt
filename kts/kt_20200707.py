# kt_20200707.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import time

"""
尝试写一个装饰器装饰之前的is_prime函数，使得当传入参数为小于2的数时提示错误信息
def is_prime(n):
    for i in range(2,n):
        if n % i == 0:
            result = False
            break
    else:
        result = True
    return result
"""

# 定义一个简单的装饰器，用来判断参数参数<2的时候提示错误信息


def decorator(func):
    def wrapper(n):
        print(f"check 【{n}】 is prime:------------begin-------------")
        t_begin = time.time()
        if n < 2:
            print("输入错误")
            result = False
        else:
            result = func(n)
        t_end = time.time()
        print(result)
        print(f"time used {t_end - t_begin}s")
        print(f"check 【{n}】 is prime:------------end---------------\n")
        return result
    return wrapper


@decorator
def is_prime(n):
    for i in range(2, n):
        if n % i == 0:
            result = False
            break
    else:
        result = True
    return result


is_prime(2)
is_prime(1)
is_prime(0)
is_prime(-1)
