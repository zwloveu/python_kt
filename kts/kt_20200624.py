# kt_20200624.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
我们会在项目1代码的基础上，添加一个新功能，同时练习循环和判断的知识。
练习要求：
新功能是：每盘（3局）游戏结束后，游戏会问我们是否要继续游戏，再加一盘。
我们可以选择再来一盘，也可以选择退出游戏。

"""

import time  # 调用time模块

import random  # 调用random模块


def pk_one_round(round_num):
    print(f"第{round_num}局开始，玩家/敌人属性如下\n")
    # 定义玩家血量
    player_life = random.randint(100, 150)
    # 定义玩家攻击
    player_attack = random.randint(30, 50)

    # 定义敌人血量
    enemy_life = random.randint(100, 150)
    # 定义敌人攻击
    enemy_attack = random.randint(30, 50)

    # 展示双方角色属性
    print("【玩家】\n" + "血量:" + str(player_life) + "\n攻击:" + str(player_attack))
    print("--------------------------------------")
    time.sleep(1)
    print("【敌人】\n" + "血量:" + str(enemy_attack) + "\n攻击:" + str(enemy_attack))
    time.sleep(1)
    print("")

    player_victory = 0
    enemy_victory = 0

    print(f"第{round_num}局战斗过程展示如下：")

    # 实现'自动战斗'
    while(player_life > 0 and enemy_life > 0):
        player_life = player_life - enemy_attack
        enemy_life = enemy_life - player_attack

        print("你发起了攻击，【敌人】剩余血量"+str(enemy_life))
        print("敌人向你发起了攻击，【玩家】剩余血量" + str(player_life))
        print("--------------------------------------")
        time.sleep(1.5)

    if (player_life > 0 and enemy_life <= 0):
        player_victory = player_victory + 1
        print("敌人死翘翘了，你赢了")
    elif (player_life <= 0 and enemy_life > 0):
        enemy_victory = enemy_victory + 1
        print("悲催，敌人把你干掉了！")
    else:
        print("哎呀，你和敌人同归于尽了！")

    print("")
    return player_victory, enemy_victory


# 判断单局结果
def show_who_is_round_winner(player_victory, enemy_victory, round_num):
    if (player_victory > enemy_victory):
        time.sleep(1)
        print(f"【第{round_num}局最终结果：你赢了！】")
    elif (enemy_victory > player_victory):
        print(f"【第{round_num}局最终结果：你输了！】")
    else:
        print(f"【第{round_num}局最终结果：平局！】")

# 判断单盘结果


def show_who_is_final_winner(player_victory_total, enemy_victory_total, round_num):
    if (player_victory > enemy_victory):
        time.sleep(1)
        print(f"【第{round_num}盘最终结果：你赢了！】")
    elif (enemy_victory > player_victory):
        print(f"【第{round_num}盘最终结果：你输了！】")
    else:
        print(f"【第{round_num}盘最终结果：平局！】")


# 三局两胜
round_number = 1
continue_flag = True
while(continue_flag):
    player_victory_total = 0
    enemy_victory_total = 0
    print(f"第{round_number}盘开始：--------------------------------------")
    for i in range(1, 4):  # 战斗三局
        time.sleep(1.5)
        player_victory, enemy_victory = pk_one_round(i)
        show_who_is_round_winner(player_victory, enemy_victory, i)
        player_victory_total = player_victory_total + player_victory
        enemy_victory_total = enemy_victory_total + enemy_victory
    show_who_is_final_winner(player_victory_total,
                             enemy_victory_total, round_number)
    print(f"第{round_number}盘结束：--------------------------------------")
    input_continue_flag = input("是否需要再来一盘：y/n(默认y，不区分大小写):")
    if(input_continue_flag.lower() != "n"):
        continue_flag = True
        round_number = round_number + 1
    else:
        continue_flag = False
        print("战斗结束")
