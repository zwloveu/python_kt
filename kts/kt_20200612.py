# kt_20200612.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
列举用1，2，3，4四个数字可以组成的所有的每位数字不重复的三位数

每位数字不重复指：个位，十位，百位数字均不相同，如123，124等，不包括111，122，121，112这类数字
"""

# 先定义一个列表，里面有1，2，3，4 四个int型元素
fourIntList = [1, 2, 3, 4]

# 再定义一个结果列表，用来保存数据
resultList = []

# 让我们来开启三个循环，在最内部循环中来比较每个循环的数字，不一样的我们就记下来
for hundersDigit in fourIntList:
    for tensDigit in fourIntList:
        for onesDigit in fourIntList:
            if (onesDigit != tensDigit and
                onesDigit != hundersDigit and
                    tensDigit != hundersDigit):
                rightNum = hundersDigit * 100 + tensDigit * 10 + onesDigit
                resultList.append(rightNum)

print(f"由1，2，3，4组成的每位都不重复的三位数有{len(resultList)}个，它们是：")
print(resultList)