# kt_20200823_12.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
第12讲作业：
公司员工基本工资1000元，门卫有高温费1000元，出差人员有补贴1000元。
张三是前台，李四是出差在外的销售，王五是门卫。求各人收入。
提示：保安和出差人员都属于公司员工。使用继承，不能在实例化后再计算。

作业提交要求：
1、脚本要结构完整，函数要有函数名
2、函数循环判断需要加注释，不要求每个语句都有，主要功能要有注释；另外就是自己觉得有助于提高代码可读性的地方也可以加注释
3、可以借鉴，不可以抄袭，抄袭一经发现，直接抄袭和被抄袭人全部打回，并且都没积分
"""

"""
3个类
    BasicSalary
    -----------
        基本工资

    HighTemperatureFeeSalary
    ------------------------
        高温费

    TravelAllowanceSalary
    ---------------------
        出差补贴
"""


class BasicSalary:
    """定义基本工资类

    属性：    
        名称
        工资(私有属性)

    方法：
        获取工资数据
    """

    def __init__(self, name):
        """初始化

        名称

        工资(私有属性)           
        """
        self.name = name
        self.__salary = 1000

    def get_salary(self, hightempcfee=0, travalallowance=0):
        return self.__salary + hightempcfee + travalallowance


class HighTempcFeeSalary(BasicSalary):
    """定义高温费类

    属性：
        高温费
    方法：
        获取工资数据，该方法重写了基本BasicSalary的get_salary
    """

    def __init__(self, name):
        BasicSalary.__init__(self, name)
        self.__high_tempc_fee = 1000

    def get_salary(self):
        return BasicSalary.get_salary(self, self.__high_tempc_fee, 0)


class TravelAllowanceSalary(BasicSalary):
    """定义出差补贴类

    属性：
        出差补贴
    方法：
        获取工资数据，该方法重写了基本BasicSalary的get_salary
    """

    def __init__(self, name):
        BasicSalary.__init__(self, name)
        self.__travel_allowance = 1000

    def get_salary(self):
        return BasicSalary.get_salary(self, 0, self.__travel_allowance)


stuff_zhangsan_qiantai = BasicSalary("张三")
stuff_lisi_xiaoshou = TravelAllowanceSalary("李四")
stuff_wangwu_menwei = HighTempcFeeSalary("王五")

print(f"{stuff_zhangsan_qiantai.name}的工资是{stuff_zhangsan_qiantai.get_salary()}")
print(f"{stuff_lisi_xiaoshou.name}的工资是{stuff_lisi_xiaoshou.get_salary()}")
print(f"{stuff_wangwu_menwei.name}的工资是{stuff_wangwu_menwei.get_salary()}")
