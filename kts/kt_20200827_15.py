# kt_20200827_15.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
第15讲作业：
将九九乘法表输出为csv文件

 

作业提交要求：
1、脚本要结构完整，函数要有函数名
2、函数循环判断需要加注释，不要求每个语句都有，主要功能要有注释；另外就是自己觉得有助于提高代码可读性的地方也可以加注释
3、可以借鉴，不可以抄袭，抄袭一经发现，直接抄袭和被抄袭人全部打回，并且都没积分

"""

import csv
import datetime
import asyncio


def num_to_cn(num):
    """return a Chinese number for a number
    """
    if num == 1:
        return "一"
    elif num == 2:
        return "二"
    elif num == 3:
        return "三"
    elif num == 4:
        return "四"
    elif num == 5:
        return "五"
    elif num == 6:
        return "六"
    elif num == 7:
        return "七"
    elif num == 8:
        return "八"
    elif num == 9:
        return "九"
    else:
        return "零"


async def write_csv(content, file, mode="w", encoding="utf-8", newline=""):
    """write content to csv file
    """
    async def write_csv_inner():
        with open(file, mode, encoding=encoding, newline=newline) as fh:
            w = csv.writer(fh)
            w.writerows(content)
    await write_csv_inner()


class MultiplicationTable:
    """九九乘法表处理类，并将结果保存为CSV文件。

    属性：

    方法：

            save,将九九乘法表数据保存为CSV文件
    """

    def __Chinese_MT_Result(self, first, second):
        """get the mt Chinese result:

            一十

            二十五

            四十九
        """
        result = first * second
        if result >= 10:
            if result % 10 == 0:
                return f"{num_to_cn(result/10)}十"
            else:
                return f"{num_to_cn((result-result%10)/10)}十{num_to_cn(result%10)}"
        else:
            return f"得{num_to_cn(result)}"

    async def save(self):
        # 总数据
        rows = []
        for i in range(1, 10):
            # 每行的数据
            inner_rows = []
            for j in range(1, i+1):
                cell_value = f"{j}x{i}={i*j}"
                print(f"{cell_value}\t", end="")
                inner_rows.append(cell_value)
            print()
            rows.append(inner_rows)


        rows_cn = []
        for i in range(1, 10):
            # 每行的数据
            inner_rows_cn = []
            for j in range(1, i+1):
                cell_value_cn =f"{num_to_cn(j)}{num_to_cn(i)}{self.__Chinese_MT_Result(j,i)}"
                print(f"{cell_value_cn}\t", end="")
                inner_rows_cn.append(cell_value_cn)
            print()
            rows_cn.append(inner_rows_cn)

        filePath = "./temp/multiplication_table_" + \
            datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S:%f")
        await write_csv(rows, filePath+".csv")
        await write_csv(rows_cn, filePath+"_cn.csv")


if __name__ == "__main__":
    mt = MultiplicationTable()
    asyncio.run(mt.save())
