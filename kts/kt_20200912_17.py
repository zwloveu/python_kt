# kt_20200912_17.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
第17讲作业：
A君目前在一家资产评估公司实习，他的工作日常就是跑遍市区里的住宅小区，调查小区的地址、建筑年份和每栋楼里每个单元里每一户的朝向和面积。

比如某一户的资料格式是：
xx花园（小区）1栋 2单元 401；朝向：南北；面积：99。
他需要把每一户的信息都录入到excel表里（csv文件）。



作业提交要求：
1、脚本要结构完整，函数要有函数名
2、函数循环判断需要加注释，不要求每个语句都有，主要功能要有注释；另外就是自己觉得有助于提高代码可读性的地方也可以加注释
3、可以借鉴，不可以抄袭，抄袭一经发现，直接抄袭和被抄袭人全部打回，并且都没积分
"""

"""
对作业的理解：
1，输入户室信息
2，保存户室信息至sqlite
3，从sqllite中查询户室信息并保存至csv
"""




import datetime
import csv
import asyncio
import os
import sqlite3
from typing import Union
from dataclasses import dataclass
async def write_csv(title: list[str], content: list[list[str]], file: str,
                    mode: str = "w", encoding: str = "utf-8", newline: str = ""):
    """write content to csv file
    Parameters:
    -----------
        title: list[str], column name for csv
        content: list[list[str]], rows contente for the csv
        file: str, csv file path
        mode: str, file open mode, default `w`
        encoding: str, file content text encoding, default `utf-8`
        newline: str, file content formatting newline, default `""`
    Returns:
    --------
    """

    async def write_csv_inner():
        with open(file, mode, encoding=encoding, newline=newline) as fh:
            w = csv.writer(fh)
            w.writerow(title)
            w.writerows(content)

    await write_csv_inner()

# house table creating script
CREATE_TABLE_HOUSE_DB_SCRIPT: str = """
CREATE TABLE IF NOT EXISTS houses (
    [id]                        INTEGER PRIMARY KEY AUTOINCREMENT,
    [housing_estate]            NVARCHAR (100) NOT NULL,
    [address]                   NVARCHAR (200) NOT NULL,
    [build_year]                INTEGER NOT NULL,
    [section_num]               NVARCHAR (50) NOT NULL,
    [unit_num]                  NVARCHAR (50) NOT NULL,
    [cell_code]                 NVARCHAR (50) NOT NULL,
    [face_to]                   NVARCHAR (10) NOT NULL,
    [area]                      FLOAT NOT NULL,
    [createdate]                DATETIME DEFAULT (datetime('now', 'localtime'))
);
"""

# data insert db script for house table
INSERT_INTO_TABLE_HOUSE: str = """
INSERT INTO houses (`housing_estate`,`address`,`build_year`,`section_num`,`unit_num`,`cell_code`,`face_to`,`area`)
VALUES (?,?,?,?,?,?,?,?)
"""

# data select db from house
SELECT_FROM_TABLE_HOUSE: str = """
SELECT `housing_estate`,`address`,`build_year`,`section_num`,`unit_num`,`cell_code`,`face_to`,`area`
FROM houses
"""

# sqlite3 - db path
HOUSE_DB_FILE: str = "./temp/2020_kt_python3.db"

# defualt csv column names
titles: list[str] = ["小区名称", "地址", "建筑年份", "楼栋", "单元", "户室", "朝向", "面积"]


@dataclass
class House:
    """House data model

    Properties:
    -----------
        housing_estate: str, 小区名称
        address: str, 地址
        build_year: int, 建筑年份
        section_num: str, 楼栋号码
        unit_num: str, 单元号码
        cell_code: str, 户室编号
        face_to: str, 朝向
        area: float, 面积
    ClassMethods:
    -------------
        by_tuple: (tuple) -> House, alternatively init method
    Methods:
    --------
        pure_data: () -> list, get pure data for the instance
        pure_data_for_csv: () -> list, get pure data for the instance and change the formatting fixed by excel
    """

    # 小区名称
    housing_estate: str
    # 地址
    address: str
    # 建筑年份
    build_year: int
    # 楼栋号码
    section_num: str
    # 单元号码
    unit_num: str
    # 户室编号
    cell_code: str
    # 朝向
    face_to: str
    # 面积
    area: float

    def __repr__(self):
        return f"{self.housing_estate}\t\t{self.address}\t\t{self.build_year}\t\t{self.section_num}\t\t{self.unit_num}\t\t{self.cell_code}\t\t{self.face_to}\t\t{self.area}"

    @classmethod
    def by_tuple(cls, data: tuple):
        """init House by data which type is tuple and contains 8 elements

        Parameters:
        -----------
            data: tuple data with all the properties to init the House instance
        """

        if not isinstance(data, tuple):
            raise TypeError("need tuple")

        if len(data) != 8:
            raise ValueError("need tuple with 8 elements")

        return cls(*data)

    def pure_data(self) -> list:
        """convert the instance to pure list data

        Parameters:
        -----------
        Returns:
        --------
            properties: list, list data by required properties
        """

        return [self.housing_estate, self.address, self.build_year,
                self.section_num, self.unit_num,
                self.cell_code, self.face_to, self.area]

    def pure_data_for_csv(self) -> list:
        """convert the instance to pure list data for csv format

        Parameters:
        ----------
        Returns:
        --------
            properties: list, list data by required properties, adpted by csv formatting
        """

        pure_data = self.pure_data()
        for k, pd in enumerate(pure_data):
            pure_data[k] = "".join(["\t", str(pd)])
        return pure_data


# Sample houses data for testing only
houses_for_test: list[House] = [
    House("罗阳一村", "罗阳路", 2002, 8, 1, "101", "西南", 100.50),
    House("罗阳一村", "罗阳路", 2002, 8, 2, "202", "西南", 137.00),
    House("罗阳一村", "罗阳路", 2002, 8, 3, "303", "西南", 120.00),
    House("罗阳一村", "罗阳路", 2002, 9, 1, "404", "西南", 100.00),
    House("罗阳一村", "罗阳路", 2002, 9, 3, "503", "西南", 180.00),
    House("罗阳一村", "罗阳路", 2002, 10, 2, "602", "西南", 95.70)
]

menus: dict[int, str] = {
    1: "录入信息",
    2: "查询信息",
    3: "退出",
}


def show_menus():
    """show command menus

    Parameters:
    -----------
    Returns:
    --------
    """

    print("################")
    for k, v in menus.items():
        print(f"{k}.{v}")
    print("################")


def basic_int_input(str_key: str) -> int:
    """basic input on the console for int type

    Parameters:
    -----------
        str_key: str, a basic message description for the input
    Returns:
    --------
        input_int_num: int, what number you input on the screen
    """

    temp_value = basic_input(str_key)
    if not temp_value.isnumeric():
        temp_value = 0
    else:
        temp_value = int(temp_value)
    return temp_value


def basic_input(str_key: str, newline: bool = False) -> str:
    """basic input on the console

    Parameters:
    -----------
        str_key: str, a basic message description for the input
        newline: bool, determin whether use a new line to input, default False
    Returns:
    --------
        input_message: str, what's you input on the console
    """

    if newline:
        temp_value = input(f"请输入{str_key}：\n")
    else:
        temp_value = input(f"请输入{str_key}：")
    return temp_value


def select_menu() -> int:
    """select the menu from the console

    Parameters:
    -----------
    Returns:
    --------
        menu_command_id: int, the selected menu command id
    """

    command_id: int = 0

    while True:
        temp_selected_command = basic_int_input("指令(1/2/3)")
        if temp_selected_command not in menus.keys():
            continue
        else:
            command_id = temp_selected_command
            break

    return command_id


def is_float(value: Union[str, int, float]) -> bool:
    """determin whether a value is float

    Parameters:
    -----------
        value: str/int/float, value to be checked
    Returns:
        result: bool, True or False
    """

    result = True
    try:
        float(value)
    except:
        result = False
    return result


def create_folder(path: str) -> str:
    """create folder if not exists

    Parameters:
    ----------
        path: str, the folder path to be created if not exists
    Returns:
    --------
        path: str, the created folder path
    """

    if not os.path.exists(path):
        os.makedirs(path)

    return path


def generate_csv_file() -> str:
    """generate a csv file name by dattime under folder ./temp

    Parameters:
    -----------
    Returns:
    -------
        filepath: str, the generated csv file path
    """

    # create folder `./temp` if not exists
    temp_path = create_folder("./temp")

    return "".join([temp_path,
                    "/houses_",
                    datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S:%f"),
                    ".csv"])


def input_house() -> list[str]:
    """input house information ,split by a comma

    Parameters:
    -----------
    Returns:
    --------
        house_fields: list[str], the house fields by user input
    """

    house_fields = []

    while True:
        house_fields.clear()
        temp_inputs = basic_input(
            '房屋信息("小区名称","地址", "建筑年份", "楼栋", "单元", "户室", "朝向", "面积"),用英文逗号隔开,系统按顺序截取所需必要数据', True).split(",")
        for field in range(len(temp_inputs)):
            if len(temp_inputs[field]) > 0:
                house_fields.append(temp_inputs[field])
        if len(house_fields) < len(titles):
            print(
                "小区名称, 地址, 建筑年份, 楼栋, 单元, 户室, 朝向, 面积。一共8个属性，用英文逗号隔开。")
            continue

        field_check_errors = []

        if not house_fields[2].isdigit():
            field_check_errors.append("建筑年份请输入数字，如2020")
        if not is_float(house_fields[7]):
            field_check_errors.append("面积请输入数字，如100/120.50")

        if len(field_check_errors) == 0:
            break
        else:
            for i in range(len(field_check_errors)):
                print(field_check_errors[i])
            continue

    return house_fields


def prepare_sqlite():
    """prepare the sqlite3 db

    Parameters:
    -----------
    Returns:
    --------
    """

    create_folder("./temp")

    connection = connect_to_db()

    connection.execute(CREATE_TABLE_HOUSE_DB_SCRIPT)

    connection.close()


def connect_to_db() -> sqlite3.Connection:
    """connect to sqlite3 db

    Parameters:
    -----------
    Returns:
        connection: sqlite3.Connection
    """

    return sqlite3.connect(HOUSE_DB_FILE)


def query_house() -> list[House]:
    """query all house data from sqlite3 db
    Parameters:
    -----------
    Returns:
    --------
        house_data: list[House], the data fetched from db, if no return empty list []
    """

    result: list[House] = []

    connection = connect_to_db()

    for row in connection.execute(SELECT_FROM_TABLE_HOUSE):
        result.append(House.by_tuple(row))

    connection.close()

    return result


def save_house_info(house_fields: list[str]):
    """save house info to sqlite3 db

    Parameters:
    -----------
        house_fields: list[str], the house fields value
    Returns:
    --------
    """

    connection = connect_to_db()

    connection.execute(INSERT_INTO_TABLE_HOUSE, tuple(house_fields))

    connection.commit()

    connection.close()


def show_houses_info(houses: list[House]):
    """show houses information on the screen

    Parameters:
    -----------
        houses: list[House], houses to show
    Returns:
    --------
    """

    print("\t\t".join(titles))

    for index in range(len(houses)):
        print(houses[index])


if __name__ == "__main__":
    async def main() -> None:
        """program main entry
        """

        while True:
            try:
                show_menus()
                command_code = select_menu()
                if command_code == 3:
                    break
                elif command_code == 1:
                    house_fields = input_house()
                    save_house_info(house_fields)
                else:
                    houses = query_house()
                    if len(houses) == 0:
                        print("目前系统中还没有数据，请选择指令1进行信息录入操作")
                    else:
                        csv_file = generate_csv_file()
                        print(f"查询数据保存在: {csv_file}")
                        show_houses_info(houses)
                        hs = []
                        for h in houses:
                            hs.append(h.pure_data_for_csv())
                        await write_csv(titles, hs, csv_file)
            except KeyboardInterrupt:
                print("用户取消了操作，系统自动退出")
                break

    prepare_sqlite()
    asyncio.run(main())
