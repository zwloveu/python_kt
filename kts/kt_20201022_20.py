# kt_20201022_20.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
第20讲作业：
实现功能：用户输入快递名称和单号，程序即可在快递100https://www.kuaidi100.com/爬取最新物流状态，并将其打印出来。

作业提交要求：
1、脚本要结构完整，函数要有函数名
2、函数循环判断需要加注释，不要求每个语句都有，主要功能要有注释；另外就是自己觉得有助于提高代码可读性的地方也可以加注释
3、可以借鉴，不可以抄袭，抄袭一经发现，直接抄袭和被抄袭人全部打回，并且都没积分

"""

import requests
import random

name = "shentong"
num = "773046451247625"

url = "https://www.kuaidi100.com/query"
headers = {
    "Host": "www.kuaidi100.com",
    "Connection": "keep-alive",
    "Accept": "application/json, text/javascript, */*; q=0.01",
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36",
    "X-Requested-With": "XMLHttpRequest",
    "Sec-Fetch-Site": "same-origin",
    "Sec-Fetch-Mode": "cors",
    "Sec-Fetch-Dest": "empty",
    "Referer": "https://www.kuaidi100.com/",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "en-US,en;q=0.9",
    "Cookie": "csrftoken=QRmM083WdwxRrHnPNmikyuRorXEFFHxwyDXZ1osiKLg; WWWID=WWWB5D9101EF8F34EFE82BCB556DCC08BB2; Hm_lvt_22ea01af58ba2be0fec7c11b25e88e6c=1603372727; __gads=ID=e021db613eba9915-22dc820453c400fd:T=1603372727:RT=1603372727:S=ALNI_MasnF-aZJnwfAakt55z7eR3tkGEDw; Hm_lpvt_22ea01af58ba2be0fec7c11b25e88e6c=1603374165"
}


params = f"type={name}&postid={num}&temp={random.random()}&phone="

whole_url = "?".join([url, params])

res = requests.get(whole_url, headers=headers)

json_info = res.json()
list_info = json_info["data"]
for info in list_info:
    print(info["context"])
    print(info["ftime"])
    print("------------------")
