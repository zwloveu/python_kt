# kt_20201015_19.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
第19讲作业：
豆瓣链接：https://movie.douban.com/top250?start=0&filter=
把豆瓣TOP250里面的 序号/电影名/评分/推荐语/链接 都爬取下来，结果就是全部展示打印出来 


作业提交要求：
1、脚本要结构完整，函数要有函数名
2、函数循环判断需要加注释，不要求每个语句都有，主要功能要有注释；另外就是自己觉得有助于提高代码可读性的地方也可以加注释
3、可以借鉴，不可以抄袭，抄袭一经发现，直接抄袭和被抄袭人全部打回，并且都没积分

"""

import requests
from bs4 import BeautifulSoup, element
from collections import namedtuple
from collections import OrderedDict

url_template: str = "https://movie.douban.com/top250?start={}&filter="
SKIP: int = 25
TOTAL: int = 250
REQUEST_HEADERS_TEMPLATE = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38",
    "Referer": url_template
}

Movie = namedtuple(
    "Movie", ["sequence", "movie_names", "score", "recommendation", "link"])


def take(start: int = 1) -> int:
    if start <= 1:
        start = 1

    total_skip = (start-1)*SKIP
    return total_skip


def build_movies(movies_info_tag: element.Tag) -> OrderedDict[int, Movie]:
    """
    get douban movie information 

    Parameters:
    -----------
        soup: a BeautifulSoup instance
    Returns:
    --------
        a Movie named tuple instance with sequence/movie_name/score/recommendation/link
    """

    movies_in_one_page: OrderedDict[int, Movie] = OrderedDict()
    for movie_info_tag in movies_info_tag.findAll("li"):
        sequence = movie_info_tag.find("em").text
        names_ti = [name.strip() for ls in [title.text.replace("\xa0", "").split("/") for title in movie_info_tag.findAll(
            "span", class_="title")] for name in ls if name.strip() != ""]
        names_ot = [name.strip() for ls in [title.text.replace("\xa0", "").split("/") for title in movie_info_tag.findAll(
            "span", class_="other")] for name in ls if name.strip() != ""]
        score = movie_info_tag.find("span", class_="rating_num").text
        recommendation = ""
        if (r := movie_info_tag.find("span", class_="inq")) != None:
            recommendation = r.text
        link = movie_info_tag.find("a").attrs["href"]

        movies_in_one_page[sequence] = Movie(
            sequence, names_ti + names_ot, score, recommendation, link)

    return movies_in_one_page


def request_movie_page(start: int = 0) -> None:
    url = url_template.format(take(start))

    request_headers = REQUEST_HEADERS_TEMPLATE
    request_headers["Referer"] = url

    response = requests.get(url=url, headers=request_headers)
    response.encoding = "utf-8"

    soup = BeautifulSoup(response.text, "html.parser")

    movie_html_sections = soup.find("ol", class_="grid_view")
    movies = build_movies(movie_html_sections)
    print(f"\t第{start+1}页的电影有：")
    for movie in movies.values():
        print(f"\t序号:\t{movie.sequence}")
        print(f"\t电影名:\t{movie.movie_names}")
        print(f"\t评分:\t{movie.score}")
        print(f"\t推荐语:\t{movie.recommendation}")
        print(f"\t链接:\t{movie.link}")
        print()


def run_kt_20201015_19() -> None:
    for i in range(int(float(TOTAL/SKIP))):
        print(f"正在爬取第{i+1}页的电影数据")
        request_movie_page(i)


if __name__ == "__main__":
    pass
