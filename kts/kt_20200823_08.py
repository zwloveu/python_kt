# kt_20200823_08.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
第8讲作业：
编写一个函数，输入一个句子，输出句子中单词的词频的字典

，。：“（）英文的逗号句号冒号单引号括号不要算到单词里面，其他的算进去不算错

比如：‘test  {test}’算成test和{test}两个单词

效果如下：
>>> words_stat('he says "he says"')
{'he': 2, 'says': 2}
"""


def words_stat(sentence):
    print("--------------------开始-----------------------")
    print("原始数据如下:\n")
    print(sentence + "\n")
    words = sentence.split(" ")
    print("切割后的列表如下:\n")
    print(words)
    print("\n")

    result_dic = {}
    # 循环列表中的元素，
    # 对于为None的不做处理
    # 非None的元素去除英文的,.:"()
    # 计算单词出现的频次，保存在变量result_dic中
    for word in words:
        if word is not None:
            result_word = remove_unused_in_word(word)
            if result_word != "":
                if result_word in result_dic:
                    result_dic[result_word] += 1
                else:
                    result_dic[result_word] = 1

    # 打印出来最终结果
    print("最终结果如下:\n")
    print(result_dic)
    print("\n")
    print("--------------------结束-----------------------\n")


# 去除英文的,.:"()
def remove_unused_in_word(word):
    temp_word = word.strip()
    temp_word = temp_word.replace(",", "")
    temp_word = temp_word.replace(".", "")
    temp_word = temp_word.replace(":", "")
    temp_word = temp_word.replace('"', "")
    temp_word = temp_word.replace("(", "")
    temp_word = temp_word.replace(")", "")
    return temp_word


words_stat("fdsfdsfdsf   \"  ffsdfsdfsd")
words_stat('  ddd .ddd    def :fw {fw}, f23 \" fdfe )few few('     )
