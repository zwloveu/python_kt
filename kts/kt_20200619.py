# kt_20200619.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
第五讲作业：
618临近，编写一个函数，传入商品价格，判断以下哪种优惠方式总价格更低
当前可以暂时不考虑error handling的部分，假设传入参数均为数字

方案一：每满300 – 40
方案二：满2件总价9折，4件及以上总价8折
"""

promotion_over300mins40_name = "每满300减40的方案"
promotion_2itemswith90percent_4items80percent_name = "2件9折4件8折的方案"


# 定义函数promotion_over300mins40，用来处理每满300减40的促销
def promotion_over300mins40(*prices):
    total = sum(prices)
    mod = total // 300
    if (mod > 0):
        total = total - 40 * mod
    return total_prices_with_digits(total)


# 定义函数promotion_2itemswith90percent_4items80percent，用来处理满2件总价9折，4件及以上总价8折
def promotion_2itemswith90percent_4items80percent(*prices):
    total = sum(prices)
    items_count = len(prices)
    if(items_count >= 4):  # 80 percent
        total = total * 0.8
    elif(items_count >= 2):  # 90 percent
        total = total * 0.9
    return total_prices_with_digits(total)


# 定义函数total_prices_with_digits，用来保留指定小数位
def total_prices_with_digits(total, digits_count=2):
    return round(total, digits_count)


def check_discount(*prices):
    print(f"打折前的总价是：{sum(prices)} 元")
    promotion_over300mins40_discount = promotion_over300mins40(*prices)
    promotion_2itemswith90percent_4items80percent_discount = promotion_2itemswith90percent_4items80percent(
        *prices)
    print(f"{promotion_over300mins40_name}的总价是：{promotion_over300mins40_discount} 元")
    print(f"{promotion_2itemswith90percent_4items80percent_name}的总价是：{promotion_2itemswith90percent_4items80percent_discount} 元")
    if(promotion_over300mins40_discount == promotion_2itemswith90percent_4items80percent_discount):
        print("价格一样啊兄弟")
    elif(promotion_over300mins40_discount > promotion_2itemswith90percent_4items80percent_discount):
        print(f"{promotion_2itemswith90percent_4items80percent_name}便宜哦")
    else:
        print(f"{promotion_over300mins40_name}便宜哦")


check_discount(100, 200.89, 300, 105)
print()
check_discount(50, 209, 89, 110.05)
print()
check_discount(50, 209, 89)
