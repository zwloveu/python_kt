# kt_20200606.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
用户输入月份，输出对应月份的天数，
如果用户输入2月，则请用户输入年份，并输出对应天数
（此处假设用户输入的月份、年份均为为数字，且年份输入正确）
"""

# 定义一个标准的12月份标准天数，2月先按28天处理，后面根据年份来处理：28/29
month_days = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31,
              6: 30, 7: 31, 8: 31, 9: 30, 10: 31, 11: 30, 12: 31}

# 定义一个变量，用来接收用户输入的月份，
# 目前不对类型做判断，假定输入的数据均为数字
month_input = input("请输入月份，1-12的数字：")

if(not month_input.isnumeric()):
    print(f"您输入的月份是：{month_input}，该值不符合，请输入月份，1-12的数字")
else:
    selected_month = int(month_input)
    if(not selected_month in month_days):
        print("请输入数字：1-12")
    else:
        print(f"您输入的月份是：{month_input}")
        if(selected_month != 2):
            print(f"{month_input}月份一共有 {month_days[selected_month]} 天")
        else:
            year_input = input("请输入年份：(大于0的正整数)")
            if(not year_input.isnumeric()):
                print(f"您输入的年份是：{year_input}，该值不符合，请输入年份：(大于0的正整数)")
            else:
                print(f"您输入的年份是：{year_input}")
                year_selected = int(year_input)
                if((year_selected % 4 == 0 and year_selected % 100 != 0) or
                   (year_selected % 400 == 0)):
                    print(
                        f"{year_input}年为闰年，{month_input}月份一共有 {month_days[selected_month]+1} 天")
                else:
                    print(
                        f"{year_input}年，{month_input}月份一共有 {month_days[selected_month]} 天")
