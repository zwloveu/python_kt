# kt_20200918_18.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
第18讲作业：
编写一个脚本，将用户输入的url对应的html页面保存到本地。同时生成一个日志文件
要求：
1.要能够处理请求出错的情况，包括请求过程出错或返回异常（如返回404等）
2.日志要包含成功以及失败的请求，日志记录的信息包括但不限于时间，请求url，请求结果

作业提交要求：
1、脚本要结构完整，函数要有函数名
2、函数循环判断需要加注释，不要求每个语句都有，主要功能要有注释；另外就是自己觉得有助于提高代码可读性的地方也可以加注释
3、可以借鉴，不可以抄袭，抄袭一经发现，直接抄袭和被抄袭人全部打回，并且都没积分
"""

import requests
import asyncio
from dataclasses import dataclass
from plupkg import file
from plupkg import input as myinput
from plupkg import time

menus: dict[int, str] = {
    1: "请求资源",
    2: "查询日志",
    3: "退出",
}


def show_menus():
    """show command menus

    Parameters:
    -----------
    Returns:
    --------
    """

    print("################")
    for k, v in menus.items():
        print(f"{k}.{v}")
    print("################")


def select_menu() -> int:
    """select the menu from the console

    Parameters:
    -----------
    Returns:
    --------
        A int value represents selected menu command id, 1/2/3
    """

    while (command_id := myinput.basic_int_input("指令(1/2/3)")) not in menus.keys():
        continue

    return command_id


@dataclass
class RequestResult:
    url: str
    status_code: int
    headers: str
    verb: str
    request_time: str

    def __str__(self):
        return f"{self.verb} {self.url} at {self.request_time}\r\n\tstatus:{self.status_code}\r\n\theaders:{self.headers}"

    async def save_log(self):
        log_path = f"./temp/requests_log-{time.get_local_date_value()}.txt"

        content: list[str] = [
            f"{self.verb} {self.url} at {self.request_time}",
            f"\tstatus:{self.status_code}",
            f"\theaders:{self.headers}"
        ]

        await file.write_text(content, log_path)


async def main() -> None:
        """program main entry
        """

        while True:
            try:
                show_menus()
                command_code = select_menu()
                if command_code == 3:
                    break
                elif command_code == 1:
                    url = myinput.basic_input("请求地址", True)

                    response = requests.get(url)

                    rr = RequestResult(
                        url, response.status_code, str(response.headers), "get", time.get_local_time_value())
                    print(rr)
                    await rr.save_log()
                else:
                    log_path = f"./temp/requests_log-{time.get_local_date_value()}.txt"
                    if not file.check_path_exists(log_path):
                        print("还没有任何请求日志记录，请重新选择指令")
                    else:
                        for line in file.read_text(log_path):
                            print(line)
            except KeyboardInterrupt:
                print("用户取消了操作，系统自动退出")
                break

def run_kt_20200918_18():
    asyncio.run(main())
